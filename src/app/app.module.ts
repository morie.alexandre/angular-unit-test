import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GestionTachesComponent } from './exo-one/gestion-taches/gestion-taches.component';
import { ExoTwoComponent } from './exo-two/exo-two.component';
import { FilterListComponent } from './exo-three/filter-list/filter-list.component';

@NgModule({
  declarations: [
    AppComponent,
    GestionTachesComponent,
    ExoTwoComponent,
    FilterListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
