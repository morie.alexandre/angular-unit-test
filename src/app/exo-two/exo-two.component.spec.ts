import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ExoTwoComponent } from './exo-two.component';

describe('ExoTwoComponent', () => {
  let component: ExoTwoComponent;
  let fixture: ComponentFixture<ExoTwoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExoTwoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExoTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // ------------------------------------------------------------------------------------------------
  //  increment function
  // ------------------------------------------------------------------------------------------------
  it('should increment counter on button click', () => {
    // Mettre en place l'environement de test
    const initialValue = component.counter;

    // Execution de la fonction
    component.increment();

    // Comportement attendu
    expect(component.counter).toBe(initialValue + 1);
  });

  // ------------------------------------------------------------------------------------------------
  //  decrement function
  // ------------------------------------------------------------------------------------------------
  it('should decrement counter on button click', () => {
    // Mettre en place l'environement de test
    const initialValue = component.counter;

    // Execution de la fonction
    component.decrement();

    // Comportement attendu
    expect(component.counter).toBe(initialValue - 1);
  });


});
