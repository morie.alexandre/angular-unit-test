import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-exo-two',
  templateUrl: './exo-two.component.html',
  styleUrls: ['./exo-two.component.scss']
})
export class ExoTwoComponent implements OnInit {

  title = 'Gestion des clicks';
  counter = 0;

  constructor() { }

  ngOnInit(): void {
  }

  /**
   * Allows to increment a counter
   */
  increment(): void {
    this.counter++;
  }

  /**
   * Allows to decrement a counter
   */
  decrement(): void {
    this.counter--;
  }



}
