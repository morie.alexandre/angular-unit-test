import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GestionTachesService {

  taskList: string[] = ['task1', 'task2'];

  constructor() { }

  addTask(task: string): void {
    this.taskList.push(task);
  }

  getTasks(): string[] {
    return this.taskList;
  }

  resetTasks(): void {
    this.taskList = [];
  }
}
