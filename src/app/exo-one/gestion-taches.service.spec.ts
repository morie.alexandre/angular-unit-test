import { TestBed } from '@angular/core/testing';
import { GestionTachesService } from './gestion-taches.service';

describe('GestionTachesService', () => {
  let service: GestionTachesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GestionTachesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('addTask', () => {
    it('should add a task to the task list', () => {

      // Mettre en place l'environement de test
      const initialTaskListLength = service.taskList.length; // Calcul de la taille initiale
      const taskToAdd = 'New Task';

      // Execution de la fonction
      service.addTask(taskToAdd);

      // Calcul de la taille updated
      const updatedTaskList = service.taskList;
      const updatedTaskListLength = service.taskList.length;

      // Comportement attendu
      expect(updatedTaskListLength).toBe(initialTaskListLength + 1);
      expect(updatedTaskList).toContain(taskToAdd);
    });
  });

});
