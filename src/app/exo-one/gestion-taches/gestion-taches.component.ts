import { Component, OnInit } from '@angular/core';
import { GestionTachesService } from '../gestion-taches.service';

@Component({
  selector: 'app-gestion-taches',
  templateUrl: './gestion-taches.component.html',
  styleUrls: ['./gestion-taches.component.scss']
})
export class GestionTachesComponent implements OnInit {

  title = 'Gestion des Taches';
  inputText = '';
  taskList: string[] = [];

  constructor(
    public gestionTachesService: GestionTachesService
  ) { }

  ngOnInit(): void {
    this.updateTaskList();
  }

  /**
   * Allows to add a task
   */
  addTask(): void {
    this.gestionTachesService.addTask(this.inputText);
    this.updateTaskList();
  }

  /**
   * Allows to get the task list
   */
  updateTaskList(): void {
    this.taskList = this.gestionTachesService.getTasks();
  }

  /**
   * Allows to reset
   */
  reset(): void {
    this.gestionTachesService.resetTasks();
    this.updateTaskList();
  }

}
