import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionTachesComponent } from './gestion-taches.component';
import { GestionTachesService } from '../gestion-taches.service';

describe('GestionTachesComponent', () => {
  let component: GestionTachesComponent;
  let fixture: ComponentFixture<GestionTachesComponent>;
  let gestionTachesServiceMock: jasmine.SpyObj<GestionTachesService>;

  beforeEach(async () => {
    // Créez un mock pour le service
    gestionTachesServiceMock = jasmine.createSpyObj('GestionTachesService', ['getTasks', 'addTask', 'resetTasks']);
    await TestBed.configureTestingModule({
      declarations: [ GestionTachesComponent ],
      providers: [{
        provide: GestionTachesService,
        useValue: gestionTachesServiceMock
    }]
    })
    .compileComponents();
  });

  beforeEach(() => {

    fixture = TestBed.createComponent(GestionTachesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // ------------------------------------------------------------------------------------------------
  //  addTask function
  // ------------------------------------------------------------------------------------------------
  it('should add a task and update the task list', () => {
    // Mettre en place l'environement de test
    component.inputText = 'New Task';
    spyOn(component, 'updateTaskList');       // Espion sur l'appel vers une méthode du composant

    // Execution de la fonction
    component.addTask();

    // Comportement attendu
    expect(gestionTachesServiceMock.addTask).toHaveBeenCalledWith('New Task');
    expect(component.updateTaskList).toHaveBeenCalled();
  });

  // ------------------------------------------------------------------------------------------------
  //  updateTaskList function
  // ------------------------------------------------------------------------------------------------
  it('should update the task list', () => {
    // Mettre en place l'environement de test
    gestionTachesServiceMock.getTasks.and.returnValue(['Task 1', 'Task 2']);

    // Execution de la fonction
    component.updateTaskList();

    // Comportement attendu
    expect(component.taskList).toEqual(['Task 1', 'Task 2']);
  });

  it('should reset tasks and update the task list', () => {

    spyOn(component, 'updateTaskList');
    component.reset();

    expect(gestionTachesServiceMock.resetTasks).toHaveBeenCalled();
    expect(component.updateTaskList).toHaveBeenCalled();
    });
});
