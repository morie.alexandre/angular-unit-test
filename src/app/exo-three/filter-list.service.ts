import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FilterListService {
  peopleList = [
    {
      nom: 'ETCHEPARE',
      prenom: 'Lilian',
      ville: 'Bayonne',
    },
    {
      nom: 'MALLET',
      prenom: 'Maxance',
      ville: 'Bordeaux',
    },
    {
      nom: 'MATHIEU',
      prenom: 'Louis-Raphael',
      ville: 'Bordeaux',
    },
    {
      nom: 'MIGNE',
      prenom: 'Pierre',
      ville: 'Toulouse',
    },
    {
      nom: 'MORIE',
      prenom: 'Alexandre',
      ville: 'Toulouse',
    },
    {
      nom: 'MOULIN',
      prenom: 'Alexandre',
      ville: 'Bayonne',
    },
    {
      nom: 'SUKHASEUM',
      prenom: 'Jonathan',
      ville: 'Toulouse',
    },
    {
      nom: 'MILCENDEAU',
      prenom: 'Bastien',
      ville: 'Bayonne',
    }
  ];

  constructor() { }

  getPeopleList(): any[] {
    return this.peopleList;
  }
}
