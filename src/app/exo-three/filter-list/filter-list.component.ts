import { Component, OnInit } from '@angular/core';
import { FilterListService } from '../filter-list.service';

@Component({
  selector: 'app-filter-list',
  templateUrl: './filter-list.component.html',
  styleUrls: ['./filter-list.component.scss']
})
export class FilterListComponent implements OnInit {

  title = 'Filtrer des personnes';
  inputText = '';
  peopleListDisplay: any[] = [];

  constructor(
    public filterListService: FilterListService
  ) { }

  ngOnInit(): void {
  }

  /**
   * Allows to filter
   */
  filter(): void {
    this.peopleListDisplay = [];
    const peopleList = this.filterListService.getPeopleList();
    peopleList.forEach((person) => {
      if (person.ville === this.inputText) {
        this.peopleListDisplay.push(person);
      }
    });
  }

}
