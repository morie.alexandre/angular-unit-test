import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterListComponent } from './filter-list.component';
import { FilterListService } from '../filter-list.service';

describe('FilterListComponent', () => {
  let component: FilterListComponent;
  let fixture: ComponentFixture<FilterListComponent>;
  let filterListServiceMock: jasmine.SpyObj<FilterListService>;

  beforeEach(async () => {
    // Créez un mock pour le service
    filterListServiceMock = jasmine.createSpyObj('FilterListService', ['getPeopleList']);
    await TestBed.configureTestingModule({
      declarations: [ FilterListComponent ],
      providers: [{
        provide: FilterListService,
        useValue: filterListServiceMock
      }]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});



